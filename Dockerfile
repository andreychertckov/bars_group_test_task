FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
ENV MAIL_LOGIN=${MAIL_LOGIN} MAIL_PASSWORD=${MAIL_PASSWORD}
ENV DB_USER=${DB_USER} DB_HOST=${DB_HOST} DB_PORT=${DB_PORT} DB_PASS=${DB_PASS} DB_NAME=${DB_NAME}
EXPOSE 80
CMD ["bash", "deploy.sh"]