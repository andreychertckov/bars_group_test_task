import os

EMAIL_HOST_USER = os.environ.get('MAIL_LOGIN')
EMAIL_HOST_PASSWORD = os.environ.get('MAIL_PASSWORD')
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.com'
EMAIL_USE_TLS = True
EMAIL_PORT = 587