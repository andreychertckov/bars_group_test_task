# Generated by Django 2.2.4 on 2019-08-31 13:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20190830_0749'),
    ]

    operations = [
        migrations.AlterField(
            model_name='result',
            name='recruit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Recruit', unique=True),
        ),
    ]
