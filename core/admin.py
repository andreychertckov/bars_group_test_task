from django.contrib import admin
from .models import Recruit, Sith, TestTask, Task, Result

# Register your models here.
admin.site.register(Recruit)
admin.site.register(Sith)
admin.site.register(TestTask)
admin.site.register(Task)
admin.site.register(Result)