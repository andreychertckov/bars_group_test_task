from rest_framework import serializers
from core.models import Sith
from . import RecruitSerializer

class SithSerializer(serializers.ModelSerializer):
    
    shadow_hands = RecruitSerializer(many=True)

    class Meta:
        model = Sith
        fields = '__all__'