from rest_framework import serializers
from core.models import TestTask, Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'text']


class TestTaskSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True)
    class Meta:
        model = TestTask
        fields = ['id', 'order_number', 'tasks']