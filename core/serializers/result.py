from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer
from core.models import Result, Answer
from . import RecruitSerializer


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ['task', 'answer']


class ResultSerializer(WritableNestedModelSerializer):
    answers = AnswerSerializer(many=True)
    recruit_data = RecruitSerializer(read_only=True, source='recruit')
    points = serializers.ReadOnlyField()
    class Meta:
        model = Result
        fields = ['recruit_data', 'tasks', 'answers', 'points', 'recruit']
