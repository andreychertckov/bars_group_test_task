from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count
from django.http import JsonResponse
from django.core.mail import send_mail
from django.conf import settings

from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from .serializers import RecruitSerializer, TestTaskSerializer, SithSerializer, ResultSerializer
from .models import Recruit, TestTask, Sith, Result

# Create your views here.
@api_view(['GET', 'POST', 'PATCH'])
def recruit(request, pk=None):
    if request.method == 'GET' and pk == None:
        recruits = Recruit.objects.all()
        serializer = RecruitSerializer(recruits, many=True)
        return JsonResponse(serializer.data, safe=False)
    elif request.method == 'GET' and pk != None:
        recruit_ = get_object_or_404(Recruit, pk=pk)
        serializer = RecruitSerializer(recruit_)
        return JsonResponse(serializer.data, safe=False)
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = RecruitSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@api_view(['GET'])
def task(request):
    task = TestTask.objects.order_by("?").first()
    serializer = TestTaskSerializer(task)
    return JsonResponse(serializer.data, safe=False)


@api_view(['GET'])
def sith(request):
    siths = Sith.objects.all()
    serializer = SithSerializer(siths, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(['GET'])
def sith_recruits(request):
    siths = Sith.objects.annotate(recruits_count=Count('shadow_hands')).filter(recruits_count__gte=1)
    serializer = SithSerializer(siths, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(['POST'])
def accept_recruits(request, sith, recruit):
    recruit_ = get_object_or_404(Recruit, pk=recruit)
    sith_ = get_object_or_404(Sith, pk=sith)
    if len(sith_.shadow_hands.all()) >= 3:
        return JsonResponse({'error': 'You can`t get more then 3 shadow hands'}, status=400)
    recruit_.accepted = True
    recruit_.mentor = sith_
    recruit_.save()
    serializer = RecruitSerializer(recruit_)
    print("Hello")
    print(settings.EMAIL_HOST_USER)
    send_mail('Sith recruting', 'You are accepted by ' + sith_.name, settings.EMAIL_HOST_USER, [recruit_.email])
    return JsonResponse(serializer.data, safe=False, status=200)

@api_view(['GET', 'POST'])
def result(request, recruit_=None):
    if request.method == 'GET':
        if recruit_ != None:
            result_ = get_object_or_404(Result, recruit=recruit_)
            serializer = ResultSerializer(result_)
            return JsonResponse(serializer.data, safe=False)
        else:
            results = Result.objects.filter(recruit__accepted=False)
            serializer = ResultSerializer(results, many=True)
            return JsonResponse(serializer.data, safe=False, status=200)
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        recruit_ = data['recruit']
        result_ = Result.objects.filter(recruit=recruit_)
        if result_:
            serializer = ResultSerializer(result_[0], data=data)
        else:
            serializer = ResultSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)