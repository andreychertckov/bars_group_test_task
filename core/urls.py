from django.urls import path

from .views import recruit, task, sith, sith_recruits, result, accept_recruits


urlpatterns = [
    path('recruit', recruit),
    path('recruit/<int:pk>', recruit),
    path('task', task),
    path('sith', sith),
    path('sith/recruits', sith_recruits),
    path('sith/<int:sith>/recruit/<int:recruit>', accept_recruits),
    path('result', result),
    path('result/<int:recruit_>', result)
]
