from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class Recruit(models.Model):
    name = models.CharField(max_length=128)
    planet =  models.CharField(max_length=256)
    age = models.PositiveIntegerField()
    email = models.EmailField()
    accepted = models.BooleanField(default=False)
    mentor = models.ForeignKey('Sith', null=True, on_delete=models.SET_NULL, related_name='shadow_hands')


class Sith(models.Model):
    name = models.CharField(max_length=128)
    planet =  models.CharField(max_length=256)


class TestTask(models.Model):
    order_number = models.CharField(max_length=128, unique=True)


class Task(models.Model):
    text = models.CharField(max_length=512)
    correct_answer = models.BooleanField()
    test_task = models.ForeignKey('TestTask', related_name='tasks', on_delete=models.CASCADE)


class Result(models.Model):
    recruit = models.OneToOneField('Recruit', on_delete=models.CASCADE)
    tasks = models.ForeignKey('TestTask', on_delete=models.CASCADE)

    @property
    def points(self):
        points_ = 0
        for a in self.answers.all():
            points_ += a.answer == a.task.correct_answer
        return points_



class Answer(models.Model):
    result = models.ForeignKey('Result', on_delete=models.CASCADE, related_name='answers')
    task = models.ForeignKey('Task', on_delete=models.CASCADE)
    answer = models.BooleanField()