python manage.py migrate
python manage.py collectstatic --noinput
echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'andreychertckov@gmail.com', '$SUPER_USER_PASS')" | python manage.py shell
gunicorn bars_group_test_task.wsgi:application --bind 0.0.0.0:80 --workers 3